import {h, Component} from 'preact';
import style from './style.css';

class TestResultPart extends Component {
    render() {
        let value = this.props.value;
        if (this.props.k.match(/time/i)) {
            value = new Date(value).toLocaleString()
        }
        return (
            <td class={style['test-result-part']}>{value}</td>
        );
    }
}

class TestResultEntryDetails extends Component {
    render() {
        return (
            <td class={style.pre} colSpan={this.props.cols}>{JSON.stringify(this.props.value, null, 2)}</td>
        );
    }
}

class TestResultEntry extends Component {

    constructor(props) {
        super(props);
        this.state = {showDetails: false};
        this.toggleDetails = this.toggleDetails.bind(this);
    }

    toggleDetails() {
        this.setState((prevState, props) => {
            return {showDetails: !prevState.showDetails}
        })
    }

    render() {
        let detailsClasses = [style['test-result-entry-details']];
        let buttonChar = '+';
        if (this.state.showDetails) {
            detailsClasses.push(style['test-result-entry-details-show']);
            buttonChar = '-';
        }
        let entry = Object.assign({}, this.props.entry);
        for (let header in this.props.selected) {
            if (!this.props.selected[header]) {
                delete entry[header];
            }
        }
        return (
            <tbody>
            <tr class={style["test-result-entry"]}>
                <td>
                    <button onClick={this.toggleDetails}>{buttonChar}</button>
                </td>
                {Object.keys(entry).map(function (field) {
                    return <TestResultPart k={field} value={entry[field]}/>
                })}
            </tr>
            <tr class={detailsClasses}>
                <TestResultEntryDetails value={this.props.entry} cols={this.props.colCount}/>
            </tr>
            </tbody>
        );
    }
}

class TestResultHeader extends Component {
    render() {
        return (
            <th class={style['test-result-header']}>{this.props.value}</th>
        );
    }
}

class TestResultListHeaders extends Component {
    render() {
        return (
            <tr className='test-result-list-header'>
                {this.props.headers.map((v) => {
                    return <TestResultHeader value={v}/>
                })}
            </tr>
        );
    }
}

class TestResultListDisplay extends Component {
    render() {
        const entries = this.props.entries;
        let er = JSON.parse(JSON.stringify(entries));
        let that = this;
        er.forEach(function (entry) {
            for (let header in that.props.selected) {
                if (!that.props.selected[header]) {
                    delete entry[header];
                }
            }
        });
        const headersBase = Array.from(new Set(er.map(Object.getOwnPropertyNames).reduce((x, y) => x.concat(y))));
        const headers = ['+'].concat(headersBase);
        return (
            <div class={style["test-result-list"]}>
                <table>
                    <thead>
                    <TestResultListHeaders headers={headers}/>
                    </thead>
                    {entries.map(function (listEntry) {
                        return <TestResultEntry selected={that.props.selected} entry={listEntry}
                                                colCount={headers.length}/>
                    })}
                </table>
            </div>
        );
    }
}

class HeaderSelectorList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const headers = this.props.entries;
        const callback = this.props.toggleHeader;
        return (
            <div class={style["header-selector-list"]}>
                <table>
                    <thead>
                    <tr>
                        <th colSpan={2}>Fields</th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(headers).map(function (header) {
                        return (<tr>
                            <td><input type='checkbox' checked={headers[header]} onClick={() => callback(header)}/></td>
                            <td>{header}</td>
                        </tr>);
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default class App extends Component {
    constructor(props) {
        super(props);
        const mockEntry = {
            'Timestamp': '2018-01-01T01:01:01Z',
            'Result': 'Passed'
        };
        const mockList = [mockEntry, mockEntry, mockEntry, mockEntry];
        const headers = new Set(mockList.map(Object.getOwnPropertyNames).reduce((x, y) => x.concat(y)));
        let headersToBeSelected = {};
        headers.forEach((x) => headersToBeSelected[x] = true);
        this.state = {
            'entries': mockList,
            'selectedHeaders': headersToBeSelected
        };
        this.toggleHeader = this.toggleHeader.bind(this);
    }

    toggleHeader(header) {
        let selectedHeaders = Object.assign({}, this.state.selectedHeaders);
        selectedHeaders[header] = !this.state.selectedHeaders[header];
        this.setState({selectedHeaders});
    }


    render() {
        return (
            <div id="app">
                <HeaderSelectorList entries={this.state.selectedHeaders} toggleHeader={this.toggleHeader}/>
                <TestResultListDisplay selected={this.state.selectedHeaders} entries={this.state.entries}/>
            </div>
        );
    }
}

package it.enbit;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Rumour.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ResultReceiverIT {

    @LocalServerPort
    public int port;

    @BeforeClass
    public static void setup() {

        String basePath = System.getProperty("server.base");
        if (basePath == null) {
            basePath = "/";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if (baseHost == null) {
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;
    }

    @Before
    public void beforeTest() {
        if (port == 0) {
            RestAssured.port = 8080;
        } else {
            RestAssured.port = port;
        }
    }

    @Test
    public void basicTest() {
        String s = given().body("{ \"result\": \"rawr\" }")
                          .contentType("application/json")
                          .expect().statusCode(200)
                          .when().post("/result").asString();
        assertThat(s.length(), is(36)); // UUID std representation is 36 chars long
    }

    @Test
    public void updateTest() {
        String uuid = given().body("{ \"result\": \"rawr\" }")
                             .contentType("application/json")
                             .post("/result").asString();

        given().body("{ \"result\": \"SuperRawr\" }")
               .contentType("application/json")
               .expect().statusCode(200)
               .when().put("/result/" + uuid);
    }

    @Test
    public void retrieveTest() {
        String json = "{ \"result\": \"rawr\" }";
        String uuid = given().body(json)
                             .contentType("application/json")
                             .post("/result").asString();

        Response response = given().accept(ContentType.JSON)
                                   .expect().statusCode(200)
                                   .when().get("/result/" + uuid)
                                   .then().contentType(ContentType.JSON).extract().response();

        String s = response.asString();

        assertThat(s, containsString(uuid));
        assertThat(s, containsString("rawr"));
    }
}

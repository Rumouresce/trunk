package it.enbit;

import it.enbit.persistance.ResultsMapDao;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class ResultReceiverTests {

    ResultReceiver sut;
    String uuid;
    Map<String, Object> m;

    @Before
    public void setUp() throws Exception {
        sut = new ResultReceiver(new ResultsMapDao());
        m = new HashMap<>();
        m.put("result", "result");
        HashMap<String, Object> r = new HashMap<>();
        r.putAll(m);
        uuid = sut.result(r);
    }

    @Test
    public void retrieveTest() {
        assertThat(sut.retrieve(uuid).result, containsString("result"));
    }

    @Test
    public void updateTest() {
        HashMap<String, Object> otherMap = new HashMap<>();
        otherMap.put("result", "other");
        sut.update(uuid, otherMap);
        assertThat(sut.retrieve(uuid).result, containsString("other"));
    }

    @Test
    public void extraFieldsTest() {
        m.put("rawr", "indeed");
        sut.update(uuid, m);

        assertThat(String.valueOf(sut.retrieve(uuid).getField("rawr")), containsString("indeed"));
    }

}

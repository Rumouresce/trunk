package it.enbit;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TestResultException extends RuntimeException {
    public TestResultException(String s) {
        super(s);
    }
}

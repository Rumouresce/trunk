package it.enbit;

import it.enbit.persistance.IResultsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.Result;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
public class ResultReceiver {

    private final IResultsDao dao;

    @Autowired
    public ResultReceiver(IResultsDao resultsDao) {
        this.dao = resultsDao;
    }

    @RequestMapping(value = "/result", consumes="application/json")
    String result(@RequestBody Map<String, Object> result) {
        TestResult tr = new TestResult(result);
        dao.make(tr);
        return tr.id.toString();
    }

    @RequestMapping(value="/result/{id}", consumes="application/json", method = RequestMethod.PUT)
    void update(@PathVariable String id, @RequestBody Map<String, Object> result) {
        UUID uuid = UUID.fromString(id);
        dao.update(new TestResult(uuid, result));
    }

    @RequestMapping(value="/result/{id}", method = RequestMethod.GET)
    public TestResult retrieve(@PathVariable String id) {
        return dao.fetchOne(UUID.fromString(id));
    }
}

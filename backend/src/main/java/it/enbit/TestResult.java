package it.enbit;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.jooq.Record3;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TestResult {
    public final UUID id;
    public String result;
    // This has to be accessible? for marshalling to json
    public Map<String, Object> attributes = new HashMap<>();

    public TestResult() {
        id = UUID.randomUUID();
    }

    public TestResult(Map<String, Object> r) {
        checkForResultField(r);
        result = String.valueOf(r.get("result"));
        r.remove("result");
        id = UUID.randomUUID();
        addAttributes(r);
    }

    @SuppressWarnings("unchecked")
    public TestResult(Record3<UUID, String, JsonElement> uuidStringObjectRecord3) {
        id = uuidStringObjectRecord3.value1();
        result = uuidStringObjectRecord3.value2();
        new Gson().fromJson(uuidStringObjectRecord3.value3(), Map.class);
        if (uuidStringObjectRecord3.value3() instanceof Map) {
            attributes = (Map<String, Object>) uuidStringObjectRecord3.value3();
        }
    }

    private void checkForResultField(Map<String, Object> r) {
        if (!r.containsKey("result") || !(r.get("result") instanceof String)) {
            throw new TestResultException("'result' non-existant or non-string");
        }
    }

    public TestResult(UUID uuid, Map<String, Object> r) {
        checkForResultField(r);
        result = String.valueOf(r.get("result"));
        r.remove("result");
        id = uuid;
        addAttributes(r);
    }

    private void addAttributes(Map<String, Object> r) {
        if (r.containsKey("id")) {
            r.remove("id");
        }
        attributes.putAll(r);
    }

    public Object getField(String key) {
        return attributes.get(key);
    }
}

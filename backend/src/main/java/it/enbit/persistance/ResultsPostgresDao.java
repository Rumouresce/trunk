package it.enbit.persistance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import it.enbit.TestResult;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.tools.json.JSONObject;
import org.postgresql.util.PGobject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

import static it.enbit.generated.Tables.RESULTS;

@Component("IResultsDao")
@Profile("postgresql")
public class ResultsPostgresDao implements IResultsDao {

    private final DSLContext create;
    private Gson gson = new GsonBuilder().create();
    private Type t = new TypeToken<Map<String, Object>>() {
    }.getType();

    @Autowired
    public ResultsPostgresDao(@Qualifier("dataSource") DataSource dataSource) throws SQLException {
        Connection connection = dataSource.getConnection();
        create = DSL.using(connection, SQLDialect.POSTGRES_9_5);
    }

    @Override
    public int make(TestResult tr) {
        JsonElement j = gson.toJsonTree(tr.attributes, t);

        return create.insertInto(RESULTS)
                     .columns(RESULTS.ID, RESULTS.RESULT, RESULTS.ATTRIBUTES)
                     .values(tr.id, tr.result, j).execute();
    }

    @Override
    public int update(TestResult tr) {
        JsonElement j = gson.toJsonTree(tr.attributes, t);
        return create.update(RESULTS)
                     .set(RESULTS.RESULT, tr.result)
                     .set(RESULTS.ATTRIBUTES, j)
                     .where(RESULTS.ID.eq(tr.id)).execute();
    }

    @Override
    public TestResult fetchOne(UUID id) {
        return new TestResult(create.select(RESULTS.ID, RESULTS.RESULT, RESULTS.ATTRIBUTES)
                                    .from(RESULTS)
                                    .where(RESULTS.ID.eq(id))
                                    .fetchOne());
    }
}


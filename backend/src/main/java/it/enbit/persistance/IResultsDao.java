package it.enbit.persistance;

import it.enbit.TestResult;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface IResultsDao {
    int make(TestResult tr);

    int update(TestResult tr);

    TestResult fetchOne(UUID id);
}

package it.enbit.persistance;

import it.enbit.TestResult;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component("IResultsDao")
@Profile("inmemory")
public class ResultsMapDao implements IResultsDao {

    private Map<UUID, TestResult> resultMap = new HashMap<>();

    @Override
    public int make(TestResult tr) {
        resultMap.put(tr.id, tr);
        return 1;
    }

    @Override
    public int update(TestResult tr) {
        resultMap.put(tr.id, tr);
        return 1;
    }

    @Override
    public TestResult fetchOne(UUID id) {
        return resultMap.get(id);
    }
}

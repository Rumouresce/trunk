package it.enbit;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class ConfigurationClass {

    @Bean
    @ConfigurationProperties(prefix = "app.datasource")
    public DataSource dataSource() throws Exception {
        return DataSourceBuilder.create().build();
    }
}
